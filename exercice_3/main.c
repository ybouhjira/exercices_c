#include <stdio.h>

int main(void)
{
    //Déclaration des variables
    int max, rang_max = 0, valeur, rang=0;
    char reponse = 'n';

    do
    {
        // Lecture de la valeur
        printf("Entrez une valeur : ");
        scanf("%d", &valeur);
        getchar();

        // Comparaison avec max
        if(rang == 0) // première valeur
            max = valeur;
        else // autre valeurs
        {
            if(valeur > max)
            {
                max = valeur;
                rang_max = rang;
            }
        }

        // Demande à l'utilsateur si il veut continuer
        printf("Voulez vous entrez une autre valeur (O/N) ? ");
        scanf("%c", &reponse);
        rang++;
    }
    while(reponse == 'O' || reponse == 'o');

    // Affichage du resultat
    printf("Le max est %d, et se trouve au rang %d\n", max, rang_max);
    return 0;
}

