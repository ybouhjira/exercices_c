#include <stdio.h>

int main(void)
{
    // lecture des nombres
    int a, b;
    printf("Entrez a : ");
    scanf("%d", &a);
    printf("Entrez b : ");
    scanf("%d", &b);

    // Affichage des resultats
    printf("%d + %d = %d  \n", a, b, a+b);
    printf("%d - %d = %d  \n", a, b, a-b);
    printf("%d * %d = %d  \n", a, b, a*b);
    printf("%d %c %d \n", a, a == b ? '=' : a < b ? '<' : '>' , b);

    return 0;
}

