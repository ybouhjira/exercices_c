#include <stdio.h>

int main(void)
{
    // Lecture des 3 nombres
    int a, b, c;
    printf("Entrez 3 nombres séparés par un espace : ");
    scanf("%d %d %d", &a, &b, &c);

    // Resultat
    printf("minimum : %d\n", a < b && a < c ? a : (b < c ? b : c));
    return 0;
}

